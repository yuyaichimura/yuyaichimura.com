# yuyaichimura.com

Uses hugo to generate file

Uses multistage docker image build image and uses docker-compose to deploy

## Pushing image to gitlab docker container registry manually

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/yuyaichimura/yuyaichimura.com .
docker push registry.gitlab.com/yuyaichimura/yuyaichimura.com
```

This pipeline is set up to build a caddy docker image and push the built image to the gitlab container registry.

After pushing the changes, run the pipeline and deploy on the target server
